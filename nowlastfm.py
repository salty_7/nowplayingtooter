# -*- coding:utf-8 -*-
# nowlastfm.py

import pylast
from settings import lastfm_api_key, lastfm_api_secret, lastfm_usrname, lastfm_password, lastfm_profile_url

class Client():
    def __init__(self):
        self.network = pylast.LastFMNetwork(api_key = lastfm_api_key, \
         api_secret = lastfm_api_secret, \
         username = lastfm_usrname, \
         password_hash = pylast.md5(lastfm_password))
        self.user = self.network.get_user(lastfm_usrname)

    def print_np(self):
        print self.get_np()

    def get_np(self):
        track = self.user.get_now_playing()
        title = u"empty"
        artist = u"empty"
        if track != None:
            title = track.title
            artist = track.artist.name
        return u"♪Nowplaying: " + title + u" / " + artist + u"\nLast.fm - " + lastfm_profile_url


if __name__ == '__main__':
    c = Client()
    c.print_np()
