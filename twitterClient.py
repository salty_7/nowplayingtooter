# -*- coding:utf-8 -*-
# twitterClient.py


import tweepy
from settings import cs_key, cs_secret, ac_key, ac_secret

class TwClient():
    def __init__(self):
        self.auth = tweepy.OAuthHandler(cs_key, cs_secret)
        self.auth.set_access_token(ac_key, ac_secret)
        self.api = tweepy.API(auth_handler=self.auth)

    def post(self, text):
        self.api.update_status(text)


if __name__ == '__main__':
    c = TwClient()
    c.post(u'てすと')
