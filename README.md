# NowPlayingTooter

## 概要

- Last.fmのNowPlayingをMasotodonやTwitterに投稿するツール。
- 依存ライブラリ：
  - Kivy
  - pylast
  - Mastodon.py
  - tweepy

## 使う前の準備

1. 日本語フォントを用意する。
  - 好きなフォントをダウンロードして、main.pyの22行目に設定する。
2. settings.pyにlast.fm, twitter, mastodonのアクセスキー等を設定する。
3. Mastodon.pyの準備として、python上で以下のコマンドを実行しておく。（詳細はMastodon.pyのreadme参照）

```python
  >>> Mastodon.create_app(CLIENTNAME, api_base_url=mstdn_api_base_url, to_file=mstdn_client_id)
  >>> mastodon = Mastodon(client_id=mstdn_client_id, api_base_url=mstdn_api_base_url)
  >>> mastodon.log_in(MAILADDRESS, PASSWORD, to_file=mstdn_access_token)
```

## スクリーンショット

![screenshot](https://media.mstdn.jp/images/media_attachments/files/007/248/347/original/6d24a39af88d5ccd.png)

https://mstdn.jp/@salty_7/100617333887282702