# -*- coding:utf-8 -*-
# main.py
import kivy

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label

import nowlastfm
import mastodonClient
import twitterClient

from kivy.config import Config
Config.set('graphics', 'width', '300')
Config.set('graphics', 'height', '500')

from kivy.core.text import LabelBase, DEFAULT_FONT
LabelBase.register(DEFAULT_FONT, "ipaexg.ttf")

class MyApp(App):
    def build(self):
        parent = Widget()
        bl = BoxLayout(size=(300,500), orientation='vertical')
        self.btn = Button(text='post')
        self.btn.bind(on_release=self.post)
        self.ti = TextInput(text=self.get_nowplaying())
        self.closeButton = Button(text='close')
        self.closeButton.bind(on_release=self.stop)
        self.reloadButton = Button(text='reload')
        self.reloadButton.bind(on_release=self.reload_np)
        self.checkbox_tw = CheckBox(active=True)
        self.checkbox_mstdn = CheckBox(active=True)
        self.labelTw = Label(text='Twitter')
        self.labelMstdn = Label(text='mstdn.jp')
        checkboxLayout = BoxLayout(orientation='horizontal')
        checkboxLayout.add_widget(self.checkbox_tw)
        checkboxLayout.add_widget(self.labelTw)
        checkboxLayout.add_widget(self.checkbox_mstdn)
        checkboxLayout.add_widget(self.labelMstdn)
        bl.add_widget(self.ti)
        bl.add_widget(checkboxLayout)
        bl.add_widget(self.btn)
        bl.add_widget(self.reloadButton)
        bl.add_widget(self.closeButton)
        parent.add_widget(bl)
        return parent

    def post(self, text):
        if self.checkbox_tw.active is True:
            self.tweet(self.ti.text)
        if self.checkbox_mstdn.active is True:
            self.toot(self.ti.text)
    
    def tweet(self, text):
        tc = twitterClient.TwClient()
        tc.post(text)
    
    def toot(self, text):
        mc = mastodonClient.Client()
        mc.post(text)

    def get_nowplaying(self):
        nlc = nowlastfm.Client()
        return nlc.get_np()

    def reload_np(self, text): 
        self.ti.text = self.get_nowplaying()


if __name__ == '__main__':
    MyApp().run()
