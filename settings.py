# -*- coding:utf-8 -*-
# settings.py

#twitter access key and token.
cs_key = u""
cs_secret = u""
ac_key = u""
ac_secret = u""

#lastfm url
lastfm_api_key = u""
lastfm_api_secret = u""
lastfm_usrname = u""
lastfm_password = u""
lastfm_profile_url = u""

#steam
steam_key = u""
steam_profile_url = u""

#mastodon(mstdn.jp)
mstdn_client_id = u""
mstdn_access_token = u""
mstdn_api_base_url = u""