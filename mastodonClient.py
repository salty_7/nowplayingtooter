# -*- coding:utf-8 -*-
# mastodonClient.py

from mastodon import Mastodon
from settings import mstdn_access_token, mstdn_api_base_url, mstdn_client_id
import warnings

warnings.simplefilter("ignore", UnicodeWarning)

class Client():
    """ mastodon用のクライアントオブジェクトを作成する。
        使用前に以下のコマンドを実行しておくこと。(詳細はMastodon.pyのreadme参照)
            Mastodon.create_app(CLIENTNAME, api_base_url=mstdn_api_base_url, to_file=mstdn_client_id)
            mastodon = Mastodon(client_id=mstdn_client_id, api_base_url=mstdn_api_base_url)
            mastodon.log_in(MAILADDRESS, PASSWORD, to_file=mstdn_access_token)
    """
    def __init__(self):
        self.mastodon = Mastodon(client_id=mstdn_client_id, 
            access_token=mstdn_access_token,
            api_base_url=mstdn_api_base_url
        )
    
    def post(self, text="test"):
        """ textをtootする。"""
        self.mastodon.toot(text)
    
    def getFirstContentTL(self):
        """ タイムライン上の最初のTootのテキストを表示する。"""
        self.mastodon.timeline(timeline="home", max_id=None, since_id=None, limit=2)[0]["content"]


if __name__ == "__main__":
    mastodon = Client()
    print mastodon.getFirstContentTL()